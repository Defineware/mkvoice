Overview:-
Based on the samples supplied by Microsoft with developing Speech Recongnition systems with creating custom voices to be used within the Windows Environment.

Requirements:-
This project has been built via Visual Studio Community 2015
Project needs to be run only in Release mode as SDK will throw build errors
Can be built in either x86 or x64 framework

Dependencies:-
Microsoft Speeck SDK 5.1 to be installed


Usage:-
********************************
Creating the Word list 
********************************
Within the text documentent add any words that have a matching audio file to be used within the voice vocabulary 
eg: audio file with the file name "test.wav" requires an entry within the word file as test

The first word on the list will always be set as the defualt word in the event that a word that isn't known is requested.
ie: if in the wordlist only has the words "yes" and "no" and the word requested is "maybe", the output will be "yes"

********************************
Audio files
********************************
The audio files are required to be within the format of 11kHz at 16bit mono
If any other format is added the code can be adjusted to suit, however testing have yield unexpected results eg: super slowed sound of the speech output

********************************
Creating the Voice file
********************************
Within the command prompt run the following command from the folder which contains the voice data and executable ie: C:\MkVoice> 

mkvoice wordlist.txt voicename.vce voicename

This will load the program mkvoice and load the wordlist to it create a file called voicename.vce and assign voicename as the voicename

The audio files and wordlist.txt must be within the same folder as the executable 

********************************
To register the voice to the PC
********************************
Should happened automatically via the codebase - see notes


********************************
To register TTSEng.dll to the pc (if required)
********************************
Within the command prompt navigate to the directory where the speech sdk is installed
C:\Program Files (x86)\Microsoft Speech SDK 5.1\Bin
and run the following command:-
regsvr32 ttsend.dll

A popup message should display confirmation, however this information has been untested


********************************
Checking Regedit
********************************
HKEY_LOCAL_MACHINE->SOFTWARE->Wow6432Node->Microsoft->Speech->Vocies->tokens

May require the computer to be reset in order for it to be fully registered
Can also navigate to C:\Windows\SysWOW64\Speech\SpeechUX and run sapi.cpl to get to the voice selector for the PC


********************************
Referance file
********************************
https://msdn.microsoft.com/en-us/library/ms720183(v=vs.85).aspx


********************************
Notes
********************************
To be able to select the desired voice via running sapi.cpl and not via control panel->Speech Recognition->text to speech as this will default it back to the anna voice
C:\Windows\SysWOW64\Speech\SpeechUX


Use as own risk as I do not guarantee any code provided thoughout