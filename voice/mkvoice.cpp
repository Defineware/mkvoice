/******************************************************************************
* mkvoice.cpp *
*-------------*
*   This application assembles a simple voice font for the sample TTS engine.
*Copyright (c) Microsoft Corporation. All rights reserved.
*
******************************************************************************/
#include "stdafx.h"
#include <direct.h>
#include "helperenum.h"

int wmain(int argc, WCHAR* argv[])
{
    USES_CONVERSION;
    static const DWORD dwVersion = { 1 };
    ULONG ulNumWords = 0;
    HRESULT hr = S_OK;

    //--- Check args
    if( argc != 4 )
    {
        printf( "%s", "Usage: > mkvoice [[in]word list file] [[out]voice file] [voice name]\n" );
        hr = E_INVALIDARG;
    }
    else
    {
        ::CoInitialize( NULL );

        //--- Open word list file and create output voice file
        //--- _wfopen is not supported on Win9x, so use fopen.
        FILE* hWordList  = fopen( W2A(argv[1]), "r" );
        FILE* hVoiceFile = fopen( W2A(argv[2]), "wb" );

        if( hWordList && hVoiceFile )
        {
            //--- Write file version and leave space for word count
            if( !fwrite( &dwVersion, sizeof(dwVersion), 1, hVoiceFile ) ||
                 fseek( hVoiceFile, 4, SEEK_CUR ) )
            {
                hr = E_FAIL;
            }

            //--- Get each entry
            WCHAR WordFileName[MAX_PATH];
            while( SUCCEEDED( hr ) && fgetws( WordFileName, MAX_PATH, hWordList ) )
            {
                ULONG ulTextLen = wcslen( WordFileName );
                if( WordFileName[ulTextLen-1] == '\n' )
                {
                    WordFileName[--ulTextLen] = NULL;
                }
                //--- Include NULL character when writing to the file
                ulTextLen = (ulTextLen+1) * sizeof(WCHAR);

                if( fwrite( &ulTextLen, sizeof(ulTextLen), 1, hVoiceFile ) &&
                    fwrite( WordFileName, ulTextLen, 1, hVoiceFile ) )
                {
                    ++ulNumWords;
                    //--- Open the wav data
                    ISpStream* pStream;
                    wcscat_s( WordFileName, L".wav" );
                    hr = SPBindToFile( WordFileName, SPFM_OPEN_READONLY, &pStream );
                    if( SUCCEEDED( hr ) )
                    {
						
                       // CSpStreamFormat Fmt;
						const SPSTREAMFORMAT spFormat = SPSF_11kHz16BitMono; // Choose any other stream format if you want, defined in SPSTREAMFORMAT
						CSpStreamFormat Fmt(spFormat, &hr);
						Fmt.AssignFormat(pStream); 
					   //if having issues with wav file that says it is a different format but has been saved correctly 
					   //test via changing the file name to something that has never been used for testing
                       if(Fmt.ComputeFormatEnum() == SPSF_11kHz16BitMono )//can swap out this line with below, however enforces standard settings
						//if(SUCCEEDED(hr))//can change to this to just enforce the format as above (spFormat)
                        {
                            STATSTG Stat;
                            hr = pStream->Stat( &Stat, STATFLAG_NONAME );
                            ULONG ulNumBytes = Stat.cbSize.LowPart;
							
							if (ulNumBytes > MAXLONG)
							{
								hr = E_OUTOFMEMORY;
							}

                            //--- Write the number of audio bytes
                            if( SUCCEEDED( hr ) &&
                                fwrite( &ulNumBytes, sizeof(ulNumBytes), 1, hVoiceFile ) )
                            {
                                //stack - overflows after about 18 files
                                //BYTE* Buff = (BYTE*)alloca( ulNumBytes );

								//dynamic stack to allow bigger files to be set
								BYTE* Buff = (BYTE*)malloc(ulNumBytes);
                                if( SUCCEEDED( hr = pStream->Read( Buff, ulNumBytes, NULL ) ) )
                                {
                                    //--- Write the audio samples
                                    if( !fwrite( Buff, 1, ulNumBytes, hVoiceFile ) )
                                    {
                                        hr = E_FAIL;
                                    }
                                }
                            }
                            else
                            {
                                hr = E_FAIL;
                            }
                        }
                        else
                        {
                            printf( "Input file: %s has wrong wav format.\r\n",  (LPSTR)CW2A( WordFileName ) );
							int errorid = Fmt.ComputeFormatEnum();
							char *error = audioenum[errorid];
							printf("Input file: %s has wrong wav format.\r\n", error);
							//just gives the error element number based on calculation
							//printf("Input file: %d has wrong wav format.\r\n", static_cast<SPSTREAMFORMAT>(error));
                        }
                        pStream->Release();
					}
					else {
						printf("Input file: %s Could not bind to file. \r\nCheck that the file name and the word spelling matches! \r\n", (LPSTR)CW2A(WordFileName));
					}
                }
                else
                {
                    hr = E_FAIL;
                }
            }
        }
        else
        {
            hr = E_FAIL;
        }

        //--- Write word count
        if( SUCCEEDED( hr ) )
        {
            if( fseek( hVoiceFile, sizeof(dwVersion), SEEK_SET ) ||
                !fwrite( &ulNumWords, sizeof(ulNumWords), 1, hVoiceFile ) )
            {
                hr = E_FAIL;
            }
        }

        //--- Register the new voice file
        //    The section below shows how to programatically create a token for
        //    the new voice and set its attributes.
        if( SUCCEEDED( hr ) )
        {
            CComPtr<ISpObjectToken> cpToken;
            CComPtr<ISpDataKey> cpDataKeyAttribs;
            hr = SpCreateNewTokenEx(
                    SPCAT_VOICES, 
                    argv[3], 
                    &CLSID_SampleTTSEngine, 
                    L"voicename", //The voice title that is required ie:bob (TokenKeyName)
                    0x409, 
                    L"voicename", //The voice title that is required ie:bob (IndependentName) relates to the name within the sapi.cpl drop down menu names
                    &cpToken,
                    &cpDataKeyAttribs);

            //--- Set additional attributes for searching and the path to the
            //    voice data file we just created.
            if (SUCCEEDED(hr))
            {
                hr = cpDataKeyAttribs->SetStringValue(L"Gender", L"Male");//male or female voice
                if (SUCCEEDED(hr))
                {
                    hr = cpDataKeyAttribs->SetStringValue(L"Name", L"voicename");//The voice title that is required ie:bob
                }
                if (SUCCEEDED(hr))
                {
                    hr = cpDataKeyAttribs->SetStringValue(L"Language", L"409");
                }
                if (SUCCEEDED(hr))
                {
                    hr = cpDataKeyAttribs->SetStringValue(L"Age", L"Adult");
                }
                if (SUCCEEDED(hr))
                {
                    hr = cpDataKeyAttribs->SetStringValue(L"Vendor", L"yourname");//who the vendor (you/company name) that the voice is to be registered against
                }

                //--- _wfullpath is not supported on Win9x, so use _fullpath.
                CHAR    szFullPath[MAX_PATH * 2];
                if (SUCCEEDED(hr) && _fullpath(szFullPath, W2A(argv[2]), sizeof(szFullPath)/sizeof(szFullPath[0])) == NULL)
                {
                    hr = SPERR_NOT_FOUND;
                }

                if (SUCCEEDED(hr))
                {
                    USES_CONVERSION;
                    hr = cpToken->SetStringValue(L"VoiceData", A2W(szFullPath));
                }
            }
        }

        //--- Cleanup
        if( hWordList  )
        {
            fclose( hWordList );
        }
        if( hVoiceFile )
        {
            fclose( hVoiceFile );
        }
        ::CoUninitialize();
    }
    return FAILED( hr );
}

